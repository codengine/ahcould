from core.views.base_template_view import BaseTemplateView
from pydrive.auth import GoogleAuth

class HomeView(BaseTemplateView):
    template_name = "home.html"

    def get(self, request, *args, **kwargs):
        response = super(HomeView, self).get(request, *args, **kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        code = self.request.GET.get('code')
        error = self.request.GET.get("error")
        if code:
            context['code'] = code
            self.request.session['code'] = code
        elif error:
            context["error"] = error
        context["gdrive_authorise_url"] = "gdrive_authorise_view"
        context["gdrive_connect_url"] = "gdrive_connect_view"
        return context