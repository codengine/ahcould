import json
from django.http.response import HttpResponseRedirect, HttpResponse
from django.conf import settings
from GDrive.gdrive_util import GDriveUtil
from core.views.base_template_view import BaseTemplateView
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


class GDriveAuthoriseView(BaseTemplateView):
    def get(self, request, *args, **kwargs):
        gauth = GoogleAuth()
        auth_url = gauth.GetAuthUrl()
        return HttpResponseRedirect(auth_url)
        # credentials = GDriveUtil.read_store_credentials()
        # auth_required = False
        # if not credentials:
        #     auth_required = True
        # else:
        #     access_token_expired = credentials.get('access_token_expired')
        #     if access_token_expired:
        #         auth_required = True
        # if auth_required:
        #     gauth = GoogleAuth()
        #     auth_url = gauth.GetAuthUrl()
        #     return HttpResponseRedirect(auth_url)
        # else:
        #     gauth_redirect_uri = settings.GDRIVE_AUTH_REDIRECT
        #     return HttpResponseRedirect(gauth_redirect_uri+"?")


class GAuthCompleted(BaseTemplateView):
    template_name = "gauth_completed.html"

    def get_context_data(self, **kwargs):
        context = super(GAuthCompleted, self).get_context_data(**kwargs)
        code = self.request.GET.get('code')
        if code:
            context['code'] = code
            self.request.session['code'] = code
        return context


class GDriveConnectView(BaseTemplateView):
    def get(self, request, *args, **kwargs):
        code = request.GET.get("auth_code")
        gauth = GoogleAuth()
        gauth.Auth(code)
        drive = GoogleDrive(gauth)
        share_email = "aamalek@alemcloud.com"
        folder_metadata = {
            'title': 'alemhealth',
            'mimeType': 'application/vnd.google-apps.folder'
        }
        folder = drive.CreateFile(folder_metadata)
        folder.Upload()
        folder_permission = folder.InsertPermission({
            'type': 'user',
            'value': share_email,
            'role': 'writer'})
        folder_id = folder['id']
        file_metadata = \
            {
                "title": "alemhealthid.txt",
                "parents":
                    [
                        {
                            "kind": "drive#fileLink",
                            "id": folder_id
                        }
                    ]
            }
        f = drive.CreateFile(file_metadata)
        f.Upload()

        # Changing folder metadata


        response = {
            "status": "SUCCESS",
            "MESSAGE": "They have been connected"
        }
        return HttpResponse(json.dumps(response))
