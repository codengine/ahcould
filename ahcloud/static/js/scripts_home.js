/**
 * Created by codenginebd on 12/10/17.
 */

$(document).ready(function () {
    var $authorise_button = $("#id_authorize_button");
    var $connect_button = $("#id_connect_button");

    function enable_authorise_button() {
        $authorise_button.prop("disabled", false);
    }

    function disable_authorise_button() {
        $authorise_button.prop("disabled", true);
    }

    function enable_connect_button() {
        $connect_button.prop("disabled", false);
    }

    function disable_connect_button() {
        $connect_button.prop("disabled", true);
    }
    
    $("#id_authorize_button").click(function (e) {
        e.preventDefault();
        var auth_url = $(this).data("auth-url");
        window.open(auth_url, "popupWindow", "width=600, height=400, scrollbars=yes");
        return false;
    });

    $("#id_connect_button").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.val("Connecting...");
        var connect_url = $(this).data("connect-url");
        var auth_code = $("#id_auth_code").val();
        call_ajax("GET", connect_url, {auth_code:auth_code}, function (data) {
            if(data.status == "SUCCESS") {
                $this.val("Connected.");
                $("#id_connected").find("span").text(data.MESSAGE);
                $("#id_connected").show();
            }
        }, 
        function (jqxhr, status, error) {
            $this.val("Connection Error");
        }, 
        function (msg) {
            
        });
        return false;
    });

    // Disable the connect button on page load
    disable_connect_button();
    
});
