import json
from pydrive.auth import GoogleAuth


class GDriveUtil(object):

    def __init__(self):
        pass

    @classmethod
    def read_store_credentials(cls, **kwargs):
        json_content = None
        with open("credentials.json", 'r') as f:
           content = f.read()
           if content:
               json_content = json.loads(content)
        return json_content

    @classmethod
    def write_store_credentials(cls, **credentials):
        try:
            creds = {}
            for key, value in credentials.items():
                creds[key] = value
            with open('credentials.json', 'w') as f:
                content = json.dumps(creds)
                f.write(content)
        except Exception as exp:
            pass


